using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;



public class PlaneFinderContentTrigger : MonoBehaviour
{

    PlaneFinderBehaviour planeFinder;
    ContentPositioningBehaviour contentPositioning;

    void Awake()
    {
        planeFinder = this.GetComponent<PlaneFinderBehaviour>();
        Debug.Assert(planeFinder);

        contentPositioning = this.GetComponent<ContentPositioningBehaviour>();
        Debug.Assert(contentPositioning);
    }


    // called as a UnityEvent
    public void PlaceContent()
    {
        planeFinder.PerformHitTest(Vector2.zero);
    }


    public void SetModelScale(float scale)
    {
        Transform anchorXf = contentPositioning.AnchorStage.transform;

        for (int i = 0; i < anchorXf.childCount; ++i)
        {
            Transform childXf = anchorXf.GetChild(i);
            childXf.localScale = Vector3.one * scale;
        }
    }
}
