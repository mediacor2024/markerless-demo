using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeppelinController : MonoBehaviour
{
    [SerializeField] float speed = 1f;

    private Vector3 targetPosition;
    private bool isFirstTime = true;


    // called as UnityEvent
    public void SetDestination(Transform target)
    {
        Debug.Assert(target != null, "target argument is null", this);
        this.gameObject.SetActive(true);

        targetPosition = target.position;
        if (isFirstTime)
        {
            // teleport the object to target position
            this.transform.position = targetPosition;
            isFirstTime = false;
        }
        else // mid-air anchor repositioned
        {
            Vector3 lookAtPoint = targetPosition;
            lookAtPoint.y = this.transform.position.y;
            this.transform.LookAt(lookAtPoint);
        }
    }


    // called as UnityEvent
    public void HideOnFirstTime()
    {
        if (isFirstTime)
            this.gameObject.SetActive(false);
    }


    void Update()
    {
        Vector3 pos = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        this.transform.position = pos;
    }

}
