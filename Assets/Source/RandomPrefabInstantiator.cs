using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class RandomPrefabInstantiator : MonoBehaviour
{
    [SerializeField] Transform xfRoot;
    [SerializeField] GameObject[] prefabs;

    bool contentWasPlaced;


    // this is called as UnityEvent
    public void InstantiateRandomPrefab()
    {
        if (contentWasPlaced)
            return;

        // obtain a random valid index
        int idx = Random.Range(0, prefabs.Length);
        // instantiate and set xfRoot as parent
        GameObject go = Object.Instantiate(prefabs[idx], xfRoot);
        // reset position/orientation to 0
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        // set the flag
        contentWasPlaced = true;
        Debug.Log($"instantiated prefab with index: {idx}");
    }


    // called by UnityEditor on inspector changes or enter Play mode
    void OnValidate()
    {
        Debug.Assert(xfRoot != null, "root xf not set", this);
        for (int i = 0; i < prefabs.Length; ++i)
            Debug.Assert(prefabs[i] != null, $"prefab not assigned at index {i}", this);
    }
}
