using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class BackButtonListener : MonoBehaviour
{

    static public bool Initialized { get; private set; }


    void Awake()
    {
        Initialized = true;
        DontDestroyOnLoad(this);
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

}
