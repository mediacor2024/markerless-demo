using System.IO;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;



public class MainMenu : MonoBehaviour
{

    [SerializeField] VerticalLayoutGroup layout;
    [SerializeField] Button btTemplate;


    void OnValidate()
    {
        Debug.Assert(layout, this);
        Debug.Assert(btTemplate, this);
    }


    void Awake()
    {
        // try create 'back' event listener
        if (!BackButtonListener.Initialized)
        {
            var go = new GameObject("Back Button Listener");
            go.AddComponent<BackButtonListener>();
        }
        // create load scene buttons
        var allScenes = FetchAllScenePaths();
        // skip scene at index 0 (main)
        for (int i = 1; i < allScenes.Length; ++i)
            CreateButton(i, allScenes[i], layout.transform);
    }


    private string[] FetchAllScenePaths()
    {
        int count = SceneManager.sceneCountInBuildSettings;
        var arr = new string[count];
        for (int i = 0; i < count; ++i)
            arr[i] = SceneUtility.GetScenePathByBuildIndex(i);
        return arr;
    }


    private void CreateButton(int idx, string path, Transform parent)
    {
        var bt = Instantiate<Button>(btTemplate, parent);
        bt.onClick.AddListener( () => SceneManager.LoadScene(idx, LoadSceneMode.Single) );
        bt.GetComponentInChildren<TextMeshProUGUI>().text = Path2ButtonText(path);
        bt.gameObject.SetActive(true);
    }


    private static string Path2ButtonText(string path)
    {
        // remove folders path and extension
        string fileName = Path.GetFileNameWithoutExtension(path);
        fileName = fileName.Replace('_', ' ');
        // capitalize first letters
        return CultureInfo.InvariantCulture.TextInfo.ToTitleCase(fileName);
    }
}
