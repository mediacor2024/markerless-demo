using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class CameraRaycaster : MonoBehaviour
{
    [SerializeField] float upscaleFactor = 1.2f;
    [SerializeField] float rotateSpeed = 100f;

    GameObject hitObject;


    void Update()
    {
        var ray = new Ray(this.transform.position, this.transform.forward);
        RaycastHit hitInfo;
        bool didHit = Physics.Raycast(ray, out hitInfo, 100f);

        GameObject go = didHit ? hitInfo.collider.gameObject : null;

        if (go != null) Debug.Log(go.name);
        if (IsTreeGameObject(go) == false)
            go = null;

        // something changed
        if (go != hitObject)
        {
            CleanupHitObject();
            SetupHitObject(go);
        }
    }


    private void SetupHitObject(GameObject newHitObject)
    {
        hitObject = newHitObject;
        if (hitObject == null)
            return;

        var objRotator = hitObject.AddComponent<ObjectRotator>();
        objRotator.Setup(Vector3.up, rotateSpeed);
        objRotator.transform.localScale = Vector3.one * upscaleFactor;
    }


    private void CleanupHitObject()
    {
        if (hitObject == null)
            return;

        hitObject.transform.localScale = Vector3.one;
        // remove component if any
        if (hitObject.TryGetComponent<ObjectRotator>(out var objRotator))
            Destroy(objRotator);
    }


    private bool IsTreeGameObject(GameObject go)
    {
        if (go == null) return false;
        return go.name.Contains("tree", System.StringComparison.InvariantCultureIgnoreCase);
    }
}
